import DefaultLayout from '../components/DefaultLayout'
import CollectionsList from '../components/CollectionsList'

export default () => (
  <DefaultLayout>
    <CollectionsList />
  </DefaultLayout>
)
