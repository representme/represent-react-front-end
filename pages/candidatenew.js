import DefaultLayout from '../components/DefaultLayout'
import CandidateNew from '../components/CandidateNew'
import axios from 'axios'
import Head from 'next/head'

const Survey = ({ collection }) => (
  <DefaultLayout>
    <Head>
      <meta property="og:title" content="Create your candidate profile" />
      <meta property="og:type" content="website" />
      <meta property="og:image" content="https://represent.me/wp-content/uploads/tmayq.jpg" />
    </Head>
    <CandidateNew />
  </DefaultLayout>
)

export default Survey
