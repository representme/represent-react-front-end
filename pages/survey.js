import DefaultLayout from '../components/DefaultLayout'
import CollectionIntro from '../components/CollectionIntro'
import axios from 'axios'
import Head from 'next/head'

const Survey = ({ collection }) => (
  <DefaultLayout>
    <Head>
      <meta property="og:title" content={collection.name} />
      <meta property="og:type" content="website" />
      <meta property="og:image" content={collection.photo} />
    </Head>
    <CollectionIntro collection={collection}/>
  </DefaultLayout>
)

Survey.getInitialProps = async ({ req }) => {
  const res = await axios.get('http://localhost:8000/api/question_collections/17/')
  const json = await res.data
  return { collection: json }
}

export default Survey
