import Document, { Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'
import injectTapEventPlugin from 'react-tap-event-plugin'
import stores from '../stores'
import axios from 'axios'

try {
  injectTapEventPlugin()
} catch(e) {}

/*

NEED TO COME UP WITH A SMART SOLUTION FOR CLIENT / SERVER ENV. OVERRIDING TO LOCALHOST FOR NOW

if (location.host === 'open.represent.me') { // Test server override defaults
  window.authSettings.facebookId = 1499361770335561;
  window.API = axios.create({
    baseURL: 'https://api.represent.me'
  });
}else if (location.host === 'share-test.represent.me' || location.host === 'test.represent.me') { // Test server override defaults
  window.authSettings.facebookId = 1684727181799018;
  window.API = axios.create({
    baseURL: 'https://test.represent.me'
  });
}else {
  window.API = axios.create({
    baseURL: 'http://localhost:8000'
  });
}

*/

export default class MyDocument extends Document {

  static getInitialProps ({ renderPage }) {
    const {html, head} = renderPage()
    return { html, head }
  }

  render () {
    return (
      <html>
        <Head>
          <link rel="stylesheet" type="text/css" href="/static/main.css" />
        </Head>
        <body>
          <Main />
          <NextScript />
          </body>
        </html>
      )
    }
}
