import DefaultLayout from '../components/DefaultLayout'
import CandidateIntro from '../components/CandidateIntro'
import axios from 'axios'
import Head from 'next/head'

const Survey = ({ collection }) => (
  <DefaultLayout>
    <Head>
      <meta property="og:title" content="Create your candidate profile" />
      <meta property="og:type" content="website" />
      <meta property="og:image" content="https://represent.me/wp-content/uploads/tmayq.jpg" />
    </Head>
    <CandidateIntro />
  </DefaultLayout>
)

export default Survey
