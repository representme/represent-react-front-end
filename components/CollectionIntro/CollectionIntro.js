import React, { Component } from 'react';
import { observer, inject } from "mobx-react";
import ReactMarkdown from 'react-markdown';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';

//import ErrorReload from '../ErrorReload';


class CollectionIntro extends Component {

  constructor() {
    super();
    this.state = {
      collection: null,
      collectionImageLoaded: false,
      networkError: false
    }
  }

  render() {
    let collection = this.props.collection

    console.log(this.props)

    let imageStyle = {
      height: '100%',
      backgroundSize: 'cover',
    }

    let outerStyle = { // Defaults no cover photo
      height: '100%',
      color: 'black',
      transition: 'all 0.5s ease-in-out',
    }

    let innerStyle = { // Defaults no cover photo
      height: '100%',
      overflow: 'scroll',
    }

    if(collection.photo) { // Override if cover photo
      outerStyle.backgroundColor = 'rgba(0,0,0,1)';
      if(this.state.collectionImageLoaded) {
        imageStyle.backgroundImage = 'url(' + collection.photo.replace("localhost:8000", "represent.me") + ')';
        outerStyle.backgroundColor = 'rgba(0,0,0,0)';
      }
      outerStyle.color = 'white';
      innerStyle.background = 'radial-gradient(ellipse at center, rgba(0,0,0,0.5) 50%,rgba(0,0,0,1) 100%)';
    }

    return (
      <div style={imageStyle}>
        <div style={outerStyle}>
          {collection.photo && <img src={collection.photo.replace("localhost:8000", "represent.me")} style={{display: 'none'}} onLoad={() => {this.setState({collectionImageLoaded: true})}} />}
          <div style={innerStyle}>
            <div style={{ display: 'table', width: '100%', height: '100%' }}>
              <div style={{ display: 'table-cell', verticalAlign: 'middle', textAlign: 'center', width: '100%', padding: '20px 20px'}}>

              <h1>{ collection.name }</h1>
              <ReactMarkdown source={ collection.desc } renderers={{Link: props => <a href={props.href} target="_blank">{props.children}</a>}} />

                <RaisedButton label="Start" primary />
                {this.props.UserStore.userData.has("id") && this.props.CollectionStore.collections.get(collection.id).user.id === this.props.UserStore.userData.get("id") && <RaisedButton label="Edit" primary />}
              </div>
            </div>
          </div>
        </div>
      </div>
    );

  }

}

export default inject("UserStore", "CollectionStore")(observer(CollectionIntro));
