import React, { Component } from 'react'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import MenuItem from 'material-ui/MenuItem';
import { Provider } from 'mobx-react';

import Stores from '../../stores';

export default class DefaultLayout extends Component {

  constructor() {
    super()

    this.state = {
      drawer: false
    }
  }

  getChildContext() {
    return { muiTheme: getMuiTheme(baseTheme) };
  }

  render() {
    return (
      <Provider
        UserStore={Stores.UserStore}
        CollectionStore={Stores.CollectionStore}
        QuestionStore={Stores.QuestionStore}
        DemographicsDataStore={Stores.DemographicsDataStore}
        CensusDataStore={Stores.CensusDataStore}
        AppStatisticsStore={Stores.AppStatisticsStore}
      >
        <div>
          <Drawer open={this.state.drawer} docked={false} onRequestChange={(drawer) => this.setState({drawer})}>
            <MenuItem>Home</MenuItem>
            <MenuItem>Test Item</MenuItem>
          </Drawer>
          {/*}<AppBar
            title="Represent"
            iconClassNameRight="muidocs-icon-navigation-expand-more"
            onLeftIconButtonTouchTap={() => this.setState({drawer: true})}
          />*/}
          <div style={{height: '100vh'}}>
            {this.props.children}
          </div>
        </div>
      </Provider>
    )
  }

}

DefaultLayout.childContextTypes = {
  muiTheme: React.PropTypes.object.isRequired,
};
