import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:8000'

/* STORES */
import UserStore from './UserStore.js';
import CollectionStore from './CollectionStore.js';
import QuestionStore from './QuestionStore.js';
import DemographicsDataStore from './DemographicsDataStore.js';
import CensusDataStore from './CensusDataStore.js';
import AppStatisticsStore from './AppStatisticsStore.js';

export default {
  UserStore:              new UserStore(),
  CollectionStore:        new CollectionStore(),
  QuestionStore:          new QuestionStore(),
  DemographicsDataStore:  new DemographicsDataStore(),
  CensusDataStore:        new CensusDataStore(),
  AppStatisticsStore:     new AppStatisticsStore(),
}
